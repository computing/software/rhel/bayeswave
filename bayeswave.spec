Name: bayeswave
Version: 1.0.11
Release: 1.1%{?dist}
Summary: LIGO/VIRGO burst analysis algorithm
License: GPLv2
Source: http://software.ligo.org/lscsoft/source/%{name}-%{version}.tar.xz
URL: https://git.ligo.org/lscsoft/bayeswave
Packager: Duncan Macleod <duncan.macleod@ligo.org>
Prefix: %{_prefix}

# -- build requires ---------

BuildRequires: gcc, gcc-c++, glibc
BuildRequires: cmake3 >= 3.12.0
BuildRequires: make
BuildRequires: help2man
BuildRequires: gsl-devel
BuildRequires: fftw-devel
BuildRequires: lal-devel
BuildRequires: lalframe-devel
BuildRequires: lalsimulation-devel
BuildRequires: lalinference-devel

Requires: fftw
Requires: gsl
Requires: lal
Requires: lalframe
Requires: lalsimulation
Requires: lalinference

# -- package definitions ----

%description
A Bayesian algorithm designed to robustly distinguish gravitational wave
signals from noise and instrumental glitches without relying on any prior
assumptions of waveform morphology.  

BayesWave produces posterior probability distributions directly on
gravitational wave signals and instrumental glitches, allowing robust,
morphology-independent waveform reconstruction.


%package devel
Summary: Files and documentation needed for compiling bayeswave programs
Requires: %{name} = %{version}
Requires: fftw-devel
Requires: gsl-devel
Requires: lal-devel
Requires: lalframe-devel
Requires: lalsimulation-devel
Requires: lalinference-devel
%description devel
A Bayesian algorithm designed to robustly distinguish gravitational wave
signals from noise and instrumental glitches without relying on any prior
assumptions of waveform morphology.

This package contains the files needed for building bayeswave programs

# -- build stages -----------

%prep
%setup -n %{name}-%{version}

%build
%cmake3 \
  -DCMAKE_BUILD_TYPE=Release .
%make_build

%install
%make_install
# man pages
mkdir -vp %{buildroot}%{_mandir}/man1
export LD_LIBRARY_PATH="%{buildroot}%{_libdir}:${LD_LIBRARY_PATH}"
help2man --source %{name} --version-string %{version} \
         --section 1 --no-info --no-discard-stderr \
         --output %{buildroot}%{_mandir}/man1/BayesWave.1 %{buildroot}%{_bindir}/BayesWave
help2man --source %{name} --version-string %{version} \
         --section 1 --no-info --no-discard-stderr \
         --output %{buildroot}%{_mandir}/man1/BayesWaveCleanFrame.1 %{buildroot}%{_bindir}/BayesWaveCleanFrame
help2man --source %{name} --version-string %{version} \
         --section 1 --no-info --no-discard-stderr \
         --output %{buildroot}%{_mandir}/man1/BayesWavePost.1 %{buildroot}%{_bindir}/BayesWavePost
help2man --source %{name} --version-string %{version} \
         --section 1 --no-info --no-discard-stderr \
         --output %{buildroot}%{_mandir}/man1/BayesWaveToLALPSD.1 %{buildroot}%{_bindir}/BayesWaveToLALPSD

%check
export LD_LIBRARY_PATH="%{buildroot}%{_libdir}:${LD_LIBRARY_PATH}"
export PKG_CONFIG_PATH="%{buildroot}%{_libdir}/pkgconfig:${PKG_CONFIG_PATH}"
# run each executable with no args (equivalent of --help)
%{buildroot}%{_bindir}/BayesWave
%{buildroot}%{_bindir}/BayesWaveCleanFrame
%{buildroot}%{_bindir}/BayesWavePost
%{buildroot}%{_bindir}/BayesWaveToLALPSD
# run pkg-config to check metadata
pkg-config --modversion bayeswave

%clean
rm -rf %{buildroot}

# -- files ------------------

%files
%{_bindir}/*
%{_libdir}/*.so.*
%{_mandir}/man1/*

%files devel
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

# -- changelog --------------

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Thu Jun 6 2019 Duncan Macleod <duncan.macleod@ligo.org> 1.3.0-1
- Rework packaging for cmake
